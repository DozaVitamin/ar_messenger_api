<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    /*
     * Метод авторизации пользователя
     *
     * @param string $access_token   фейсбук токен, который юзер получил при авторизации
     * @param string $facebook_id    фейсбук айди пользователя
     *
     * return void
     */
    public function userlogin($access_token = '', $facebook_id = '') {
        $this->load->model('user_model');

        $access_token = (string) $access_token;
        $facebook_id = (string) $facebook_id;

        $result = array(
            'response_status' => 'error',
            'message' => 'Unable to verify user.',
        );

        if (empty($access_token) || empty($facebook_id)) {
            echo json_encode($result);
            return;
        }

        $json = @file_get_contents("https://graph.facebook.com/me?access_token={$access_token}");
        if (!strpos($http_response_header[0], "200")) {
            echo json_encode($result);
            return;
        }

        $content = json_decode($json);
        $auth_data['facebook_id'] = $content->id;
        $auth_data['name'] = $content->name;

        if ($facebook_id != $auth_data['facebook_id']) {
            echo json_encode($result);
            return;
        }

        $authorization_result = $this->user_model->authorization($auth_data);

        if ($authorization_result) {
            $result = array(
                'response_status' => 'success',
                'message' => 'User successfully verified.',
            );
        } else {
            $result['message'] = 'Server error occured when tried to authorize user.';
        }

        echo json_encode($result);
        return;
    }

    /*
     * Метод публикации сообщения
     *
     * @param string $message       текст сообщения
     * @param string $usergroup     группа пользователей, которая сможет просмотреть сообщение
     * @param string $radius        радиус видимости сообщения
     * @param string $location_lat  текущая широта
     * @param string $location_lon  текущая долгота
     *
     * return void
     */
    public function post_message($message = '', $usergroup = '', $radius = '', $location_lat = '', $location_lon = '') {
        $this->load->model('message_model');

        $message = (string) $message;
        $usergroup = (string) $usergroup;
        $radius = (string) $radius;
        $location_lat = (string) $location_lat;
        $location_lon = (string) $location_lon;
        $user_id = $this->session->userdata('user_id');

        $result = array(
            'response_status' => 'error',
            'message' => 'Unable to post message',
        );

        if (empty($message) || empty($usergroup) || empty($radius) || empty($location_lat) || empty($location_lon) || empty($user_id)) {
            echo json_encode($result);
            return;
        }

        $data = array(
            'creator_user_id' => $user_id,
            'usergroup_id' => $usergroup,
            'radius' => $radius,
            'message' => $message,
            'location_lat' => $location_lat,
            'location_lon' => $location_lon,
        );

        $insert_result = $this->message_model->post_message($data);

        if (!$insert_result) {
            echo json_encode($result);
            return;
        }

        $result = array(
            'response_status' => 'success',
            'message' => 'Message has been successfully posted',
        );
        echo json_encode($result);
        return;
    }

    /*
     * Метод получения доступных пользователю сообщений
     *
     * @param array $current_lat    текущая широта
     * @param array $current_lon    текущая долгота
     *
     * return void
     */
    public function get_messages($current_lat = '', $current_lon = '') {
        $this->load->model('message_model');

        $current_lat = (string) $current_lat;
        $current_lon = (string) $current_lon;
        $user_id = $this->session->userdata('user_id');

        $result = array(
            'response_status' => 'error',
            'data' => array(),
        );

        if (empty($current_lat) || empty($current_lon) || empty($user_id)) {
            echo json_encode($result);
            return;
        }

        $data = $this->message_model->get_messages($current_lat, $current_lon, $user_id);

        $result = array(
            'response_status' => 'success',
            'data' => $data,
        );
        echo json_encode($result);
        return;
    }
}

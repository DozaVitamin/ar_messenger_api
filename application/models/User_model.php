<?php
/**
 * Модель пользователя
 *
 * Работа с пользователями приложения
 *
 * PHP Version 7.1.0
 *
 * @package Models
 * @author  LightSide <a.balan@netpeak.net>
 */
/***/
class User_model extends CI_Model{

    /** Сonstructor	*/
    public function __construct(){
        parent::__construct();
    }

    /*
     * Получение информации о пользователе
     *
     * @param string $facebook_id   фейсбук айди пользователя
     *
     * return array
     */
    private function getUser($facebook_id = '') {
        $facebook_id = (string) $facebook_id;

        if (empty($facebook_id)) {
            return array();
        }

        $this->db->where('facebook_id', $facebook_id);
        $result = $this->db->get('users')->row_array();

        if (!isset($result)) {
            return array();
        }

        return $result;
    }

    /*
     * Авторизация пользователя
     *
     * @param array $auth_data   фейсбук айди пользователя
     *
     * return bool
     */
    public function authorization(array $auth_data = array()) {
        if (empty($auth_data) || !isset($auth_data['facebook_id']) || !isset($auth_data['name'])) {
            return false;
        }

        $facebook_id = $auth_data['facebook_id'];
        $name = $auth_data['name'];

        if (empty($facebook_id) || empty($name)) {
            return false;
        }

        $user = $this->user_model->getUser($facebook_id);

        if (empty($user) || $name != $user['name']) {
            $sql = "INSERT INTO users (facebook_id, name) VALUES ('{$facebook_id}', '{$name}') ON DUPLICATE KEY UPDATE name = '{$name}'";
            $this->db->query($sql);
            $user = $this->user_model->getUser($facebook_id);
        }

        $this->session->set_userdata("user_id", $user['id']);

        return true;
    }

}

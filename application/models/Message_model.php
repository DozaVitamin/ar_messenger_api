<?php
/**
 * Модель пользователя
 *
 * Работа с пользователями приложения
 *
 * PHP Version 7.1.0
 *
 * @package Models
 * @author  LightSide <a.balan@netpeak.net>
 */
/***/
class Message_model extends CI_Model{

    /** Сonstructor	*/
    public function __construct(){
        parent::__construct();
    }

    /*
     * Публикация сообщения
     *
     * @param array $data   данные о сообщении
     *
     * return bool
     */
    public function post_message(array $data = array()) {

        if (empty($data)) {
            return false;
        }

        $this->db->insert('messages', $data);
        $insert_id = $this->db->insert_id();

        if ($insert_id) {
            return true;
        }

        return false;
    }

    /*
     * Получение доступных сообщений
     *
     * @param array $current_lat    текущая широта
     * @param array $current_lon    текущая долгота
     * @param array $user_id        айди юзера
     *
     * return array
     */
    public function get_messages($current_lat = '', $current_lon = '', $user_id = '') {
        $current_lat = (string) $current_lat;
        $current_lon = (string) $current_lon;
        $user_id = (string) $user_id;


        if (empty($current_lat) || empty($current_lon) || empty($user_id)) {
            return array();
        }

        $return_array = array();

        $this->db->where('usergroup_id',2);
        $this->db->or_where('creator_user_id', $user_id);
        $result = $this->db->get('messages')->result_array();

        if (empty($result)) {
            return array();
        }

        foreach ($result as $message) {
            $distance = $this->haversineGreatCircleDistance($current_lat, $current_lon, $message['location_lat'], $message['location_lon']);

            if ($distance < $message['radius'] + 50) {
                $return_array[] = $message;
            }
        }

        return $return_array;
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     *
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     *
     * @return float Distance between points in [m] (same as earthRadius)
     */
    private function haversineGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $angle * $earthRadius;
    }

}
